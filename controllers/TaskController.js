const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return error
		}

		return savedTask
	})
}
// Get All Task
module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

// Update Task
module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.name = new_data.name 

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}
// Delete Task
module.exports.deleteTask = (task_id) => {
	return Task.findByIdAndDelete(task_id).then((removedTask, error) => {
		if(removedTask == removedTask) {
			return 'Successfully deleted the task!'
		}

		if(!removedTask){
		
			return 'Task not found'
		}

		return removedTask
	})
}

// ACTIVITY
// 2. Get Single task
module.exports.getSingleTasks = (task_id) => {
	return Task.findById(task_id).then((result) => {
		return result
	})
}

// 6. Change Status
module.exports.completeTasks = (task_id, new_data) => {
	return Task.findByIdAndUpdate(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.status = new_data.status

		return result.save().then((completedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return completedTask
		})
	})
}