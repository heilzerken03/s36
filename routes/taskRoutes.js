const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// Create single task
router.post('/create', (request, response) => {
	// *MVC - Model - View - Controller
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

// Update a task
router.patch('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// Delete task - MINI ACTIVITY
/*
	1. Setup a route for deleting a task
	2. Setup the controller for deleting a task
	3. Build the controller function responsible for deleting a task
	4. Send the result of that controller function as a response

	Endpoint: '/:id/delete'
*/

router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// ACTIVITY
// 1. Get Single Task
router.get('/:id/find', (request, response) => {
	TaskController.getSingleTasks(request.params.id).then((result) => {
		response.send(result)
	})
})

// 5. Change Status
router.patch('/:id/complete', (request, response) => {
	TaskController.completeTasks(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// 
module.exports = router