const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
})
// module.exports - to be exported to other files or used by other files
module.exports = mongoose.model('Task', taskSchema)